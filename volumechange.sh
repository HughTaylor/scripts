#!/bin/sh
# script for increasing the volume of the current sink

die() {
	echo "$1"
	exit "$2"
}

# Get the current sink
sink=$(pactl get-default-sink)

# make sure 1 or more args were given
[ $# -eq 0 ] && die "Usage: $0 [up|down]" 1

# test the first arg, and set sink.
if [ "$1" == "up" ]; then
	pactl set-sink-volume $sink +5%
fi

if [ "$1" == "down" ]; then
	pactl set-sink-volume $sink -5%
fi
