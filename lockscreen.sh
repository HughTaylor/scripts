#!/bin/sh
# a demnu prompt script
# Gives demnu promp labeled with $1
#
# can echo desired state (eg "mem") to  /sys/power/state
# alternatively can use systemctl
# systemctl requires root
# can update sudoers file with visudo to allow access
# eg allow wheel group members to use sudo systemctl suspend and reboot without password:
# %wheel ALL=(ALL:ALL) NOPASSWD: /usr/bin/systemctl suspend reboot

choices="lock\nsleep\nreboot\ncancel"

chosen=$(echo -e "$choices" | dmenu -i -p "$1")

case "$chosen" in
	lock) slock ;;
	sleep) slock & sudo systemctl suspend ;;
	reboot) sudo systemctl reboot ;;
esac
