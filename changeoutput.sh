#!/bin/sh
# a demnu prompt script
# Gives demnu promp labeled with $1
#
# list cards with "pacmd list-cards | less"
# available profiles listed under "profiles" section

choices="headphones\noptical\ncancel"

chosen=$(echo -e "$choices" | dmenu -i -p "$1")

case "$chosen" in
	optical) pactl set-card-profile alsa_card.pci-0000_2f_00.4 output:iec958-stereo ;;
	headphones) pactl set-card-profile alsa_card.pci-0000_2f_00.4 output:analog-stereo ;;
esac
